$(document).ready(inicio);
//se inicializa la funcion para cargar los datos de las tablas relacionadas//
function inicio() {
	 // cargar datos de consulta en el modal de nuevo registro//
    cargarDatos();
    // cargar datos de paciente en el modal de nuevo registro de consulta//
    cargarPaciente();
    // cargar datos de doctor en el modal de nuevo registro de consulta//
    cargarDoctor();
    // cargar datos de doctor en el modal para editar registro de consulta//
    cargarDoctor2();
    // cargar datos de paciente en el modal para editar registro de consulta//
    cargarPaciente2()
    // evento click con el boton guardar para el nuevo registro de consulta//
    $("#guardarConsulta").click(guardar);
    // evento click con el boton eliminar registro de consulta//
    $("#eliminarConsulta").click(eliminar);
    // evento click con el boton editar registro de consulta//
    $("#modificarConsulta").click(modificar);

}

// METODO PARA CARGAR DATOS CON UNA PETICION AJAX
// EN LA TABLA.
function cargarDatos() {
    // peticion AJAX al back-end
    $.ajax({
        url: "/consultas/all",
        method: "Get",
        data: null,
        success: procesarDatosTabla,
        error: errorPeticion
    });
}
//procesando datos de la peticion	
function procesarDatosTabla(response) {
    // se procesa la respuesta del back-end o servidor
    // y se agregar los valores obtenidos en la respuesta
    // a la tabla, a su tbody
    // resetear datos de la tabla
    $("#tDatos").html("");
    response.forEach(item => {
        $("#tDatos").append(""
            + "<tr>"
            + "<td>" + item.id + "</td>"
            + "<td>" + item.fecha + "</td>"
            + "<td>" + item.sintomas + "</td>"
            + "<td>" + item.diagnostico + "</td>"
            + "<td>" + item.doctor.nombre + "</td>"
            + "<td>" + item.paciente.nombre + "</td>"
            + "<td>"
            + "<button onclick='preModificar(" + item.id + ")'  type='button' class='btn btn-warning ml-4 mt-1' data-toggle='modal' data-target='#editar'>Editar</button>"
            + "<button onclick='preEliminar(" + item.id + ");' type='button' class='btn btn-danger ml-4 mt-1' data-toggle='modal' data-target='#eliminar'>Eliminar</button>"
            + "</td>"
            + "</tr>");

    });

}
//mostrando un alert en la cual se especifica que hay un error en la peticion (error de syntaxis o programacion)
function errorPeticion(response) {
    alert("Error al realizar la peticion: " + response);
    console.log("Error al realizar la peticion: " + response);
}
//cargar datos de la tabla relacionada (pacientes) en el modal de nuevo registro de consulta//
function cargarPaciente() {
    $.ajax({
        url: "/pacientes/all",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#paciente").append(""
                    + '<option value=' + item.id + '>' + item.nombre + '</option>'

                );
            });
        },
        error: errorPeticion
    });
}
//cargar datos de la tabla relacionada (docentes) en el modal de nuevo registro de consulta//
function cargarDoctor() {
    $.ajax({
        url: "/doctores/all",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#doctor").append(""
                    + '<option value=' + item.id + '>' + item.nombre + '</option>'

                );
            });
        },
        error: errorPeticion
    });
}
//guardar un nuevo registro de consulta//
function guardar() {
    $.ajax({
        url: "/consultas/save",
        method: "Get",
        data: {
            fecha: $("#fecha").val(),
            sintomas: $("#sintomas").val(),
            diagnostico: $("#diagnostico").val(),
            idDoctor: $("#doctor").val(),
            idPaciente: $("#paciente").val()
        },
        success: function (response) {

            alert(response.mensaje);
            cargarDatos();
        },
        error: errorPeticion

    });
}
//confirmacion de eliminacion del registro//
function preEliminar(id) {
    $("#idConsulta").val(id);
    $("#modalEliminarConsulta").modal();
}
//funcion para eliminar el registro//
function eliminar() {
    var id = $("#idConsulta").val();
    $.ajax({
        url: "/consultas/delete/" + id,
        method: "Get",
        data: null,
        success: function (response) {
            alert(response.mensaje);
            cargarDatos();
        },
        error: errorPeticion

    });
}
//cargar datos de doctor en el modal para editar el registro de consulta//
function cargarDoctor2() {
    $.ajax({
        url: "/doctores/all",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#doctor2").append(""
                    + '<option value=' + item.id + '>' + item.nombre + '</option>'
                );


            });
        },
        error: errorPeticion
    });
}
//cargar datos de paciente en el modal para editar el registro de consulta//
function cargarPaciente2() {
    $.ajax({
        url: "/pacientes/all",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#paciente2").append(""
                    + '<option value=' + item.id + '>' + item.nombre + '</option>'
                );


            });
        },
        error: errorPeticion
    });
}
//funcion para cargar los datos en el modal y ser modificados//
function preModificar(id) {
    $("#modalModificarConsulta").modal();
    // alert(id);
    $.ajax({
        url: "/consultas/getConsulta/" + id,
        method: "Get",
        success: function (response) {
            $("#id").val(response.id);
            $("#fecha2").val(response.fecha);
            $("#sintomas2").val(response.sintomas);
            $("#diagnostico2").val(response.diagnostico);
            $("#doctor2").val(response.doctor.id);
            $("#paciente2").val(response.paciente.id);
        },
        error: errorPeticion

    });
}
//funcion para modificar el registro de consulta//
function modificar() {
    var id = $("#id").val();
    $.ajax({
        url: "/consultas/update/" + id,
        method: "Get",
        data: {
            id: id,
            fecha: $("#fecha2").val(),
            sintomas: $("#sintomas2").val(),
            diagnostico: $("#diagnostico2").val(),
            idDoctor: $("#doctor2").val(),
            idPaciente: $("#paciente2").val()
        },
        success: function (response) {
            alert(response.mensaje);
            cargarDatos();
        },
        error: errorPeticion

    });
}