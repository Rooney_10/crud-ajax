$(document).ready(inicio);
function inicio() {
	// cargar datos de paciente en el modal de nuevo registro//
    cargarDatos();
    // evento click con el boton guardar para el nuevo registro de paciente//
    $("#guardarPaciente").click(guardar);
    // evento click con el boton eliminar un registro de doctor//
    $("#eliminarPaciente").click(eliminar);
    // evento click con el boton editar para el registro de doctor//
    $("#modificarPaciente").click(modificar);
}

// METODO PARA CARGAR DATOS CON UNA PETICION AJAX
// EN LA TABLA.
function cargarDatos() {
    // peticion AJAX al back-end
    $.ajax({
        url: "/pacientes/all",
        method: "Get",
        data: null,
        success: procesarDatosTabla,
        error: errorPeticion
    });
}
// procesando datos de la peticion

function procesarDatosTabla(response) {
    // se procesa la respuesta del back-end o servidor
    // y se agregar los valores obtenidos en la respuesta
    // a la tabla, a su tbody
    // resetear datos de la tabla
    $("#tDatos").html("");
    response.forEach(item => {
        $("#tDatos").append(""
            + "<tr>"
            + "<td>" + "" + item.id + "</td>"
            + "<td>" + "" + item.nombre + "</td>"
            + "<td>" + "" + item.direccion + "</td>"
            + "<td>"
            + "<button onclick='preModificar(" + item.id + ");' class='btn btn-warning ml-2'>Modificar</button>"
            + "<button onclick='preEliminar(" + item.id + ");' class='btn btn-danger ml-2'>Eliminar</button>"
            + "</td>"
            + "</tr>"
            + "");
    });
}
//mostrando un alert en la cual se especifica que hay un error en la peticion (error de syntaxis o programacion)
function errorPeticion(response) {
    alert("Error al realizar la peticion: " + response);
    console.log("Error al realizar la peticion: " + response);
}
// cargar datos en el modal de nuevo registro de paciente//
function cargarPaciente() {
    $.ajax({
        url: "http://localhost:8080/pacientes/save",
        method: "Get",
        data: {
            nombre: $("#nombre").val(),
            direccion: $("#direccion").val()
        },
        success: function (response) {
            alert(response.mensaje);
            cargarDatos();
        },
        error: errorPeticion

    });
}
//function para guardar un nuevo registro paciente//
function guardar(){
	$.ajax({
        url:"/pacientes/save",
        method:"Get",
        data:{
            nombre:$("#nombre").val(),
            direccion:$("#direccion").val()
            
        },
        success:function(response){
	
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
//confirmacion de eliminacion del registro de paciente//
function preEliminar(id) {
	$("#idPaciente").val(id);
	$("#modalEliminarPaciente").modal();
  }
  function eliminar() {
    var id=$("#idPaciente").val();
    $.ajax({
        url:"/pacientes/delete/"+id,
        method:"Get",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
// cargar los datos en el select para modificar el registro de paciente//
function preModificar(id) {
    $("#modalModificarPaciente").modal();
    // alert(id);
    $.ajax({
        url:"/pacientes/getPaciente/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id),
            $("#nombre2").val(response.nombre),
            $("#direccion2").val(response.direccion)
        },
        error:errorPeticion

    });
}
// funcion para modiifcar el registro de paciente//
function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"/pacientes/update/"+id,
        method:"Get",
        data:{
            id:id,
            nombre: $("#nombre2").val(),
            direccion: $("#direccion2").val(),
            idPaciente:$("#paciente2").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}