
$(document).ready(inicio);
function inicio() {
	cargarDatos();

	$("#guardarEspecialidad").click(guardar);
	$("#eliminarEspecialidad").click(eliminar);
   $("#modificarEspecialidad").click(modificar);

}

//METODO PARA CARGAR DATOS CON UNA PETICION AJAX
//EN LA TABLA.
function cargarDatos() {
	// peticion AJAX al back-end
	$.ajax({
		url: "/especialidades/all",
		method: "Get",
		data: null,
		success: procesarDatosTabla,
		error:errorPeticion
	});
}
//procesando datos de la peticion	

		function procesarDatosTabla(response) {
			// se procesa la respuesta del back-end o servidor
			// y se agregar los valores obtenidos en la respuesta
			// a la tabla, a su tbody
			// resetear datos de la tabla
			$("#tDatos").html("");
			response.forEach(item => {
				$("#tDatos").append(""
				+"<tr>"
					+"<td>"+""+item.id+"</td>"
					+"<td>"+""+item.especialidad+"</td>"
					+"<td>"
						+"<button onclick='preModificar("+item.id+");' class='btn btn-warning ml-2'>Modificar</button>"
						+"<button onclick='preEliminar("+item.id+");' class='btn btn-danger ml-2'>Eliminar</button>"
					+"</td>"
				+"</tr>"
				+"");
			});
		}
function errorPeticion(response) {
	alert("Error al realizar la peticion: " + response);
	console.log("Error al realizar la peticion: " + response);
}
//cargar datos en el mmodal de nuevo registro de especialidad//
function cargarEspecialidad() {
    $.ajax({
        url: "/especialidades/save",
        method: "Get",
        data:{
            especialidad:$("#especialidad").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
function guardar(){
	$.ajax({
        url:"/especialidades/save",
        method:"Get",
        data:{
            especialidad:$("#especialidad").val(),
            
        },
        success:function(response){
	
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
function preEliminar(id) {
	$("#idEspecialidad").val(id);
	$("#modalEliminarEspecialidad").modal();
  }
  function eliminar() {
    var id=$("#idEspecialidad").val();
    $.ajax({
        url:"/especialidades/delete/"+id,
        method:"Get",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
function preModificar(id) {
    $("#modalModificarEspecialidad").modal();
    // alert(id);
    $.ajax({
        url:"/especialidades/getEspecialidad/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            $("#especialidad2").val(response.especialidad);
        },
        error:errorPeticion

    });
}
function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"/especialidades/update/"+id,
        method:"Get",
        data:{
            id:id,
            especialidad:$("#especialidad2").val(),
            idEspecialidad:$("#especialidad2").val()
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
