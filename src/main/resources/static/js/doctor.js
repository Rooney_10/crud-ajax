$(document).ready(inicio);
// se inicializa la funcion para cargar los datos de las tablas relacionadas//
function inicio() {
    // cargar datos de doctor en el modal de nuevo registro//
    cargarDatosDoctor();
    // cargar datos de especialialidades en el modal de nuevo registro//
    cargarEspecialidades();
    // cargar datos de especialidades en el modal para editar el registro//
    cargarEspecialidades2();
    // evento click con el boton guardar para el nuevo registro de doctor//
    $("#guardarDoctor").click(guardar);
  // evento click con el boton eliminar un registro de doctor//
    $("#eliminarDoctor").click(eliminar);
  // evento click con el boton editar para el registro de doctor//
    $("#modificarDoctor").click(modificar);
}

// METODO PARA CARGAR DATOS CON UNA PETICION AJAX
// EN LA TABLA.
function cargarDatosDoctor() {
    // peticion AJAX al back-end
    $.ajax({
        url: "/doctores/all",
        method: "Get",
        data:null,
        success: procesarDatosTabla,
        error: errorPeticion
    });
}
// procesando datos de la peticion
function  procesarDatosTabla(response) {
            // se procesa la respuesta del back-end o servidor
            // y se agregar los valores obtenidos en la respuesta
            // a la tabla, a su tbody
            // resetear datos de la tabla
	        // se recorren los elementos del array
            // retornado como respuesta
            $("#tdatos").html("");
            response.forEach(item => {
                $("#tdatos").append(""
                    + "<tr>"
                    + "<td>" + item.id + "</td>"
                    + "<td>" + item.nombre + "</td>"
                    + "<td>" + item.direccion + "</td>"
                    + "<td>" + item.especialidad.especialidad + "</td>"
                    + "<td>"
                    + "<button onclick='preModificar(" + item.id + ")'  type='button' class='btn btn-warning ml-4 mt-1' data-toggle='modal' data-target='#editar'>Editar</button>"
                    + "<button onclick='preEliminar(" + item.id + ");' type='button' class='btn btn-danger ml-4 mt-1' data-toggle='modal' data-target='#eliminar'>Eliminar</button>"
                    + "</td>"
                    + "</tr>");

            });
}
//mostrando un alert en la cual se especifica que hay un error en la peticion (error de syntaxis o programacion)
function errorPeticion(response) {
    alert("Error al realizar la peticion: " + response);
    console.log("Error al realizar la peticion: " + response);
}
// cargar datos de la tabla relacionada (especialidad) en el modal de nuevo registro de doctor//
function cargarEspecialidades() {
    $.ajax({
        url: "/doctores/allEspecialidad",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#especialidad").append(""
                    + '<option value=' + item.id + '>' + item.especialidad + '</option>'
                );


            });
        },
        error: errorPeticion
    });
}
// cargar datos en el modal  de la tabla relacionada (especialidad) para editar el registro de doctor//
function cargarEspecialidades2() {
    $.ajax({
        url: "/doctores/allEspecialidad",
        method: "Get",
        data: null,
        success: function (response) {
            response.forEach(item => {
                $("#especialidad2").append(""
                    + '<option value=' + item.id + '>' + item.especialidad + '</option>'
                );


            });
        },
        error: errorPeticion
    });
}
// function para guardar un nuevo registro doctor//
function guardar() {
    $.ajax({
        url: "/doctores/save",
        method: "Get",
        data: {
            nombre: $("#nombre").val(),
            direccion: $("#direccion").val(),
            idEspecialidad: $("#especialidad").val()
        },
        success: function (response) {
            alert(response.mensaje);
            cargarDatosDoctor();
        },
        error: errorPeticion

    });
}
// confirmacion de eliminacion del registro de doctor//
function preEliminar(id) {
    $("#idDoctor").val(id);
    $("#modalEliminarDoctor").modal();
}
// funcion para eliminar el registro de doctor//
function eliminar() {
    var id = $("#idDoctor").val();
    $.ajax({
        url: "/doctores/delete/" + id,
        method: "Get",
        data: null,
        success: function (response) {
            alert(response.mensaje);
            cargarDatosDoctor();
        },
        error: errorPeticion

    });
}
// funcion de confirmacion de la eliminacion del registro de doctor//
function preModificar(id) {
    $("#modalModificarDoctor").modal();
    // alert(id);
    $.ajax({
        url: "/doctores/getDoctor/" + id,
        method: "Get",
        success: function (response) {
            $("#id").val(response.id);
            $("#nombre2").val(response.nombre);
            $("#direccion2").val(response.direccion);
            $("#especialidad2").val(response.especialidad.id);
        },
        error: errorPeticion

    });
}
//funcion para modificar el registro de doctor//
function modificar() {
    var id = $("#id").val();
    $.ajax({
        url: "/doctores/update/" + id,
        method: "Get",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            direccion: $("#direccion2").val(),
            idEspecialidad: $("#especialidad2").val()
        },
        success: function (response) {
            alert(response.mensaje);
            cargarDatosDoctor();
        },
        error: errorPeticion

    });
}
