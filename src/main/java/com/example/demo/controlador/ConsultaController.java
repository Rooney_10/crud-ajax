package com.example.demo.controlador;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.entidad.Consulta;
import com.example.demo.entidad.Doctor;
import com.example.demo.entidad.Paciente;
import com.example.demo.servicios.ConsultaService;
import com.example.demo.servicios.DoctorService;

@Controller
//Dando permisos para hacer peticiones desde afuera del proyecto//
@CrossOrigin
@RequestMapping("consultas")// nombre del controlador//
public class ConsultaController {
	
	// repositorio para manipular los datos de la base//
	@Autowired
	ConsultaService consultaService;
	@Autowired
	DoctorService doctorService;
	
	@GetMapping(value="index")
    public String index() {
        return new String("views/Consulta/consulta");
    }

	
///Metodo para listar y mostrarlo en JSON, o un tabla//
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Consulta>getAllConsultas(){
		return (List<Consulta>) consultaService.getAll();
	}
	//llamando los campos de la tabla pacientes (por medio de serviceConsulta) para cargarlas en el select para nuevo registro de consulta//
	public List<Paciente>getAllPaciente(){
		return (List<Paciente>) consultaService.getAll1();
	}
	//llamando los campos de la tabla doctores (por medio de serviceConsulta)  para cargarlas en el select para nuevo registro de consulta//
	public List<Doctor>getAllDoctor(){
		return (List<Doctor>) consultaService.getAll2();
	}
	//listar registros  por id de la tabla consulta en el modal para editarlo//
    @GetMapping(value="getConsulta/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Consulta getConsulta(@PathVariable Integer id) {

        return  consultaService.getConsulta(id);
    }
	
	 //listar registros  por id de la tabla doctores en el modal para editarlo//
    @GetMapping(value="getDoctor/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Doctor getDoctor(@PathVariable Integer id) {

        return  consultaService.getDoctor(id);
    }
  //listar registros  por id  de la tabla pacientes en el modal para editarlo//
    @GetMapping(value="getPaciente/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Paciente getPaciente(@PathVariable Integer id) {

        return  consultaService.getPaciente(id);
    }
    
    //metodo para guardar un nuevo registro de consulta//
	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> save(
			@RequestParam  Date fecha,
			@RequestParam String sintomas,
			@RequestParam String diagnostico,
			@RequestParam Integer idDoctor,
			@RequestParam Integer idPaciente){
		Consulta co = new Consulta();//creando objeto de consulta
		
		HashMap<String, String> jsonReturn = new HashMap<>();
		// asignado datos al objeto de consulta
		co.setFecha(fecha);
		co.setSintomas(sintomas);
		co.setDiagnostico(diagnostico);
		co.setDoctor(consultaService.getDoctor(idDoctor));
		co.setPaciente(consultaService.getPaciente(idPaciente));
		try {
			consultaService.SaveOrUpate(co);// guardando registro de consulta
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro guardado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada (error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
		
	}
	//metodo para eliminar un registro de consulta//
	@GetMapping(value = "delete/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> delete(@PathVariable Integer id) {

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			// buscando registro
			Consulta co = consultaService.getConsulta(id);
			// eliminando registro
			consultaService.Delete(co);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada (error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}
	//metodo para actualzar un registro de consulta//
		@GetMapping(value = "update/{id}")
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> update(
	            @RequestParam Integer id,
	            @RequestParam Date fecha,
				@RequestParam String sintomas,
				@RequestParam String diagnostico,
				@RequestParam Integer idDoctor,
				@RequestParam Integer idPaciente) {
			Consulta co = new Consulta();// creando objeto de consulta//

			HashMap<String, String> jsonReturn = new HashMap<>();
			// asignado datos al objeto de coonsulta
	        co.setId(id);
			co.setFecha(fecha);
			co.setSintomas(sintomas);
			co.setDiagnostico(diagnostico);
			co.setDoctor(consultaService.getDoctor(idDoctor));
			co.setPaciente(consultaService.getPaciente(idPaciente));

			// manejando cualquier excepcion de error de la peticion
			try {
				consultaService.SaveOrUpate(co);// guardando registro de doctor
				jsonReturn.put("estado", "OK");
				jsonReturn.put("mensaje", "Registro actualizado");
				return jsonReturn;

			} catch (Exception e) {
				jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada (error en la programacion del codigo)//
				jsonReturn.put("mensaje", "Registro no actualizado" + e.getMessage());
				return jsonReturn;
			}
		}	
}
