package com.example.demo.controlador;

import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entidad.Doctor;
import com.example.demo.entidad.Especialidad;

import com.example.demo.servicios.DoctorService;

@Controller  
//Dando permisos para hacer peticiones desde fgit fuera del proyecto.//
@CrossOrigin
@RequestMapping("doctores") // nombre del controlador//git
public class DoctorController {

	// repositorio para manipular los datos de la base //
	@Autowired
	DoctorService doctorService;
	
	@GetMapping(value="index")
    public String index() {
        return new String("views/Doctor/doctor");
    }
	//Metodo para listar y mostrarlo en JSON, o un tabla//
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Doctor> ListaDoctor() {
		return (List<Doctor>) doctorService.getAll();
	}
	 //listar registros de doctor  por id en el modal para editarlo//
    @GetMapping(value="getDoctor/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public Doctor getDoctor(@PathVariable Integer id) {

        return  doctorService.getDoctor(id);
    }
	//llamando los campos de la tabla especialidades para cargarlas en el select para nuevo registro de doctores//
	//listar registros de la tabla relacionada con doctores para devolver la data en la peticion//
    @GetMapping(value="allEspecialidad",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public List<Especialidad> getAllEspecialidad() {
        return (List<Especialidad>) doctorService.getAllEspecialidad();
    }

    //metodo para guardar elementos de un nuevo registro de doctor como un arreglo//
	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> save(
			@RequestParam String nombre,
			@RequestParam String direccion,
			@RequestParam Integer idEspecialidad) {
		
		Doctor doctor = new Doctor();// se crea el objeto doctor

		HashMap<String, String> jsonReturn = new HashMap<>();
		// asignado datos al objeto de doctor
		doctor.setNombre(nombre);
		doctor.setDireccion(direccion);
		doctor.setEspecialidad(doctorService.getEspecialidad(idEspecialidad));

		// manejando cualquier excepcion de error

		try {
			doctorService.SaveOrUpate(doctor);// guardando registro de doctor
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro guardado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada (error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}

//metodo para eliminar un registro de doctores//
	@GetMapping(value = "delete/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> delete(@PathVariable Integer id) {

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			// buscando registro//
			Doctor doctor = doctorService.getDoctor(id);
			// eliminando registro//
			doctorService.Delete(doctor);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada en jquery(error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}

// metodo para actualizar un registro de doctores//
	@GetMapping(value = "update/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> update( 
			@RequestParam Integer id,
			@RequestParam String nombre,
			@RequestParam String direccion,
			@RequestParam Integer idEspecialidad) {
		Doctor doctor = new Doctor();// creando objeto de doctor

		HashMap<String, String> jsonReturn = new HashMap<>();
		// asignado datos al objeto de doctor
		doctor.setId(id);
		doctor.setNombre(nombre);
		doctor.setDireccion(direccion);
		doctor.setEspecialidad(doctorService.getEspecialidad(idEspecialidad));

		// manejando cualquier excepcion de error
		try {
			doctorService.SaveOrUpate(doctor);// guardando registro de doctor
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro actualizado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada en jquery (error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no actualizado" + e.getMessage());
			return jsonReturn;
		}
	}
}
