package com.example.demo.controlador;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entidad.Especialidad;
import com.example.demo.entidad.Paciente;
import com.example.demo.repositorio.IPacienteRepository;
import com.example.demo.servicios.ConsultaService;

@Controller
//Dando permisos para hacer peticiones desde afuera del proyecto//
@CrossOrigin
@RequestMapping("pacientes")// nombre del controlador//
public class PacienteController {
	@Autowired
	IPacienteRepository rpaciente;
	@Autowired
	ConsultaService consultaService;
	
	@GetMapping(value="index")
    public String index() {
        return new String("views/Paciente/paciente");
    }
    //metodo para listar los datos de la tabla pacientes mostrando un json o una tabla//
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Paciente> getAll(){
		return (List<Paciente>) rpaciente.findAll();
	}
	//listar registros  de pacientes por id en el modal para editarlo//
	    @GetMapping(value="getPaciente/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseBody
	    @CrossOrigin
	    public Paciente getPaciente(@PathVariable Integer id) {

	        return  consultaService.getPaciente(id);
	    }
	    
     //metodo para guardar elementos de un nuevo registro de paciente como un arreglo//
	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> save(
			@RequestParam String nombre,
			@RequestParam String direccion) {
		Paciente pa = new Paciente();// creando objeto de paciente//

		HashMap<String, String> jsonReturn = new HashMap<>();
		// asignado datos al objeto de paciente//
		pa.setNombre(nombre);
		pa.setDireccion(direccion);

		// manejando cualquier excepcion de error
		try {
			rpaciente.save(pa);// guardando registro de paciente//
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro guardado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada (error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}
	// metodo para eliminar un registro de pacientes//
	@GetMapping(value = "delete/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> delete(@PathVariable Integer id) {

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			// buscando registro//
			Paciente pa = rpaciente.findById(id).get();
			// eliminando registro//
			rpaciente.delete(pa);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;

		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada en jquery(error en la programacion del codigo)//
			jsonReturn.put("mensaje", "Registro no guardado" + e.getMessage());
			return jsonReturn;
		}
	}
	// metodo para actualizar un registro de pacientes//
		@GetMapping(value = "update/{id}")
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> update(@RequestParam Integer id, 
		@RequestParam String nombre,
		@RequestParam String direccion) {
			Paciente pa = new Paciente();// creando objeto de paciente//

			HashMap<String, String> jsonReturn = new HashMap<>();
			// asignado datos al objeto de paciente//
			pa.setId(id);
			pa.setNombre(nombre);
			pa.setDireccion(direccion);

			// manejando cualquier excepcion de error//
			try {
				rpaciente.save(pa);// guardando registro de paciente//
				jsonReturn.put("estado", "OK");
				jsonReturn.put("mensaje", "Registro actualizado");
				return jsonReturn;

			} catch (Exception e) {
				jsonReturn.put("estado", "ERROR");//mensaje de error en la peticion realizada en jquery (error en la programacion del codigo)//
				jsonReturn.put("mensaje", "Registro no actualizado" + e.getMessage());
				return jsonReturn;
			}
		}
}
